# Data_exchange



This repository contains a collection of functions that can be used as a library for the transformation of simulated data of brain dynamics at different scales. The set of functions is generated from a limited collection of known information exchange use cases. The scales which are covered by these functions are morphologically detailed cell models, point neuron models and whole brain models. 
