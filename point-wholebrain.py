
import numpy as np

from quantities import ms, Hz
from neo.core import AnalogSignal, SpikeTrain

from elephant.statistics import instantaneous_rate
from elephant.kernels import RectangularKernel

from elephant.spike_train_generation import StationaryPoissonProcess, NonStationaryPoissonProcess


def spiketrain_to_rate(start, stop, dt, spiketrain):
    """
    Implements the abstract method for the transformation of the
    spike trains to rate.
    Applicable to, among others:
     SpikeEvents in NEST/Arbor/Neuron to Rate state variables in TVB
     SpikeEvents in NEST/Arbor/Neuron to
         RateEvent/InstantaneousRateConnectionEvent/DelayedRateConnectionEvent in NEST

    :param start: int - start time
    :param stop: int - stop time
    :param dt: float - time delta
    :param spiketrain: neo.core.SpikeTrain - data structure containing the spike train to be transformed into rate
    :return: rate: float rate for the interval where data is provided
    """

    rate = instantaneous_rate(spiketrain,
                               t_start=np.around(start, decimals=2) * ms,
                               t_stop=np.around((stop, decimals=2) * ms,
                               sampling_period=(dt - 0.000001) * ms, kernel=RectangularKernel(1.0 * ms))
    return rate


def rate_to_spikes(start, stop, rate, stationary):
    """
    Transform rate data into spike trains
    Applicable to, among others:
     Rate state variables in TVB to SpikeEvents in NEST/Arbor/Neuron
     RateEvent/InstantaneousRateConnectionEvent/DelayedRateConnectionEvent in NEST
        to SpikeEvents in NEST/Arbor/Neuron

    :param start: int - start time of the spiking interval
    :param stop: int - end time of the spiking interval
    :param rate: float - the rate of the signal
    :param stationary: bool - if the Poisson Process should be stationary or not
    :return: neo.core.SpikeTrain - spike train
    """


    rate += 1e-12
    rate = np.abs(rate)  # avoid rate equal or below zero
    spike_train = []
    if stationary:
        spike_train = StationaryPoissonProcess(rate * Hz, t_start=start * ms, t_stop = stop * ms)
    else:
        spike_train = NonStationaryPoissonProcess(rate * Hz, t_start=start * ms, t_stop=stop * ms)
    return spike_train


def rate_to_phase(rate, start, stop):
    """
    Function to calculate the phase from an Analog signal produced with a specific rate
    Applicable to, among others:
     Rate state variables in TVB to Phase state variables in TVB
    :param rate: float - the rate of the original signal
    :param start: int - start time of the signal
    :param stop: int - start time of the signal
    :return: float array - phase of the signal generated
    """
    sig = AnalogSignal(rate * Hz, t_start=start * ms,
                          sampling_period=stop * ms)
    phase = np.atan2(sig.signal[0],sig.signal[1])
    return phase

def rate_to_order(rate, start, stop):
    """
    Function to calculate the oder parameter from an Analog signal produced
    with a specific rate
    Applicable to, among others:
     Rate state variables in TVB to Phase state variables in TVB
    :param rate: float - the rate of the original signal
    :param start: int - start time of the signal
    :param stop: int - start time of the signal
    :return: float array - oder parameter of the signal generated
    """
    sig = AnalogSignal(rate * Hz, t_start=start * ms,
                          sampling_period=stop * ms)
    phase = np.atan2(sig.signal[0],sig.signal[1])
    order = np.abs(np.mean(np.exp(1j * phase), axis=0))
    return order



'''
NOTE
current_to_spiketrain:
Applicable to, among others:
 Current state variables in TVB to SpikeEvents in NEST/Arbor/Neuron
 CurrentEvent/DSCurrentEvent in NEST to SpikeEvents in NEST/Arbor/Neuron

Using a spike generator/cell model in NEST/Arbor/Neuron directly connected to the
input region and which is able to receive current events is the 
most suitable solution.
'''



