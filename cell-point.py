
import numpy as np

def calcium_to_rate(spiketrain, beta_Ca = 0.001, tau_Ca = 10000.0):
    """
    Function to transform calcium concentration into instantaneous firing rate
    :param spiketrain: the spike timestamps
    :param beta_Ca: the calcium intake constant
    :param tau_Ca: the calcium decay constant
    :return: rate
    """
    Ca_minus = 0.0

    for t in spiketrain:
        Ca_minus = Ca_minus * np.exp((Ca_t_ - t) / tau_Ca)
        if Ca_minus < 0.0:
            Ca_minus = 0.0
        Ca_t_ = t
        Ca_minus += beta_Ca

    rate = Ca_minus*beta_Ca*tau_Ca
    return rate


def spike_to_spike(spiketrain, morphology_segments, distribution):
    """
    Distributes a spike train from a single synapse to entries along a dentritic morphology

    :param spiketrain: list of spike times
    :param morphology_segments: int array
    :param distribution: type of distribution used to split the spikes [round, random, progression]
    :return: spiketrain_morpho: lists of spike times distributed along the morphology
    """
    c_segments = len(morphology_segments)
    spiketrain_morpho = []*c_segments
    if distribution=="round":
        count = 0
        for s in spiketrain:
            spiketrain_morpho[count%len(c_segments)].append([s])
    elif distribution=="random":
        for s in spiketrain:
            spiketrain_morpho[np.randint(len(c_segments))].append([s])
    else:
        #distribution=="progression"
        items = len(spiketrain)/c_segments
        for i in enumerate(c_segments):
            spiketrain_morpho[i] = spiketrain[items*i, (items*i)+ items]

    return spiketrain_morpho
